FROM php:7.2-apache

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y zlib1g-dev unzip zip
RUN docker-php-ext-install zip opcache
RUN a2enmod rewrite

RUN curl --insecure https://getcomposer.org/composer.phar -o /usr/bin/composer && chmod +x /usr/bin/composer

COPY ./docker/000-default.conf /etc/apache2/sites-available/000-default.conf

WORKDIR /var/www/html

# Copy just composer json/lock files, so composer install layer can be cached
COPY ./composer.json ./composer.lock ./
RUN composer install --no-scripts --no-autoloader

COPY ./ ./

# After all sources are added, we want to run dump-autoload and any scripts defined in composer.json
RUN composer dump-autoload --optimize && composer run-script post-install-cmd

CMD ["apache2-foreground"]

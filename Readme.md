WEB app for TODO list project
=============================

This repository contains a PHP application that serves React based fronted for TODO list project.
PHP side is responsible for authentication part and redirecting requests from React app to core project written in JAVA.

`assets` folder contains all React app code
Other directories in this repo come from PHP Symfony framework.

# Build instructions 

1. Checkout and build backend API project (https://bitbucket.org/mechpave/todo-java-backend/src/master/)
2. Run `docker-compose up`
3. App should be accessible at `http://localhost`

User credentials (there are two pre-registered users):
- `test@test.com` @ `test`
- `test2@test.com` @ `testas`

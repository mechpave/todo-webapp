import * as TodoActions from './todoActions'

export function TodoListReducer(state = [], action) {
    switch (action.type) {

        case TodoActions.GET_TODOS_SUCCESS: {
            return action.todos.data;
        }

        default:
            return state
    }
}

import { ApiClient } from "../apiClient";

export const GET_TODOS_SUCCESS = 'GET_TODOS_SUCCESS';

export function createTodo(todo) {
    return (dispatch, getState) => {
        return ApiClient.createTodo(todo)
            .then(res => {
                dispatch(GetTodos());
            })
            .catch((error) => {
                alert('We could not finish your request. Please check your input data');
            })
        }
}

export function toggleTodo(id) {
    return (dispatch, getState) => {
        return ApiClient.toggleTodo(id)
            .then(res => {
                dispatch(GetTodos());
            })
            .catch((error) => {
                alert('Your todo item was not updated. Please try again later');
            })
    }
}

export function GetTodos() {
    return (dispactch, getState) => {
        return ApiClient.getTodos()
            .then(res => {
                dispactch(GetTodoSuccess(res));
            })
            .catch((error) => {
                alert('Your todo items could not be retrieved. Please try again later');
            })
    }
}

export function GetTodoSuccess(todos) {
    return {
        type:GET_TODOS_SUCCESS,
        todos
    }
}

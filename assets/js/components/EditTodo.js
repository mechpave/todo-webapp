import React, {Component} from 'react';
import DatePicker from "react-datepicker";

class EditTodo extends Component {

    constructor(props) {
        super(props);

        this.state = {
            ...this.emptyTodo()
        };

        this.createTodo = this.createTodo.bind(this);
        this.resetTodo = this.resetTodo.bind(this);
        this.changeTitle = this.changeTitle.bind(this);
        this.changeDeadline = this.changeDeadline.bind(this);
    }

    emptyTodo () {
        return {title: "", deadline: null}
    };

    createTodo (event) {
        this.resetTodo();
        this.props.createTodo(this.state);
    };

    resetTodo () {
        this.setState({title: "", deadline: null})
    };

    changeTitle (event) {
        this.setState({title: event.target.value})
    }

    changeDeadline (date) {
        const chosenDate = new Date(date);
        const formatted_date = chosenDate.getFullYear() + "-"
            + this.appendLeadingZeroes(chosenDate.getMonth() + 1) + "-"
            + this.appendLeadingZeroes(chosenDate.getDate());

        this.setState({deadline: formatted_date})
    }

    appendLeadingZeroes(n){
        if(n <= 9){
            return "0" + n;
        }

        return n;
    }

    render() {
        return (
            <div>
                <div className={"input-group mb-3"}>
                    <input type="text"
                           className="form-control"
                           aria-label="Text input with checkbox"
                           value={this.state.title}
                           placeholder='Title'
                           onChange={this.changeTitle}
                    />

                    <DatePicker
                        dateFormat="yyyy-MM-dd"
                        className="form-control"
                        placeholderText="Pick a deadline"
                        onChange={this.changeDeadline}
                        selected={this.state.deadline ? new Date(this.state.deadline) : null}
                        minDate={new Date()}
                    />

                    <div className="input-group-append" id="button-addon4">
                        <button className="btn btn-outline-secondary"
                                type="button"
                                onClick={this.createTodo}>
                            Create
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default EditTodo;

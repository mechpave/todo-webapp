import React, {Component} from 'react';
import DataTable from 'react-data-table-component';

import EditTodo from './EditTodo'

class TodoList extends Component {

    constructor(props) {
        super(props);

        this.toggleTodo = this.toggleTodo.bind(this);
    }

    toggleTodo (data) {
        this.props.toggleTodo(data.id);
    };

    render() {
        const columns = [
            {
                name: 'ID',
                selector: 'id',
                sortable: true,
                center: true
            },
            {
                name: 'Title',
                selector: 'title',
                sortable: true,
                center: true
            },
            {
                name: 'Deadline',
                selector: 'deadline',
                sortable: true,
                center: true
            },
            {
                name: 'Created at',
                selector: 'createdAt',
                sortable: true,
                center: true
            },
            {
                name: 'Completed',
                selector: 'completed',
                sortable: true,
                format: row => row.completed ? 'Yes' : 'No',
                center: true
            }
        ];

        return (
            <div>
                <EditTodo createTodo={this.props.createTodo} />

                <DataTable
                    title="Todo items"
                    columns={columns}
                    data={this.props.todos}
                    pagination={true}
                    highlightOnHover={true}
                    pointerOnHover={true}
                    onRowClicked= {data => this.toggleTodo(data)}
                />
            </div>
        )
    }
}

export default TodoList;

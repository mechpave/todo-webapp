import axios from 'axios'

const getTodos = () => {
    return axios('/api/todo-items');
};

const createTodo = todo => {
    return axios.post('/api/todo-items', todo);
};

const toggleTodo = id => {
    return axios.patch('/api/todo-items/'+id+'/toggle');
};

const ApiClient = {getTodos, createTodo, toggleTodo};

export {ApiClient}

import React, { Component } from 'react';
import * as TodoActions from '../redux/todoActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import TodoList from '../components/todoList';

export class TodoContainer extends Component {
    constructor(props) {
        super(props);

        this.createTodo = this.createTodo.bind(this);
        this.toggleTodo = this.toggleTodo.bind(this);
    }

    createTodo(todo) {
        this.props.actions.createTodo(todo)
    };

    toggleTodo(id) {
        this.props.actions.toggleTodo(id)
    };

    render() {
        return (
            <div className="todo-container">
                <TodoList
                    todos={this.props.todos}
                    createTodo={this.createTodo}
                    toggleTodo={this.toggleTodo}
                />
            </div>
        );
    }
}

TodoContainer.propTypes = {
    actions: PropTypes.object.isRequired,
    todos: PropTypes.array.isRequired
};

function mapStateToProps(state, ownProps) {
    return {
        todos: state.todos
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(TodoActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoContainer);

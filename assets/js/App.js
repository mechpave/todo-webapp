import React, { Component } from 'react';
import { Provider } from 'react-redux'
import { configureStore } from './redux/configureStore'
import * as TodoActions from './redux/todoActions'

import TodoContainer from './containers/todoContainer'

const store = configureStore();

store.dispatch(TodoActions.GetTodos());

const App = (props) => {
    return (
        <Provider store={store} >
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="#">TODO LIST</a>
                <a className="navbar-brand float-right" href="/logout">Logout</a>
            </nav>
            <div className={'clearfix'} />

            <hr />

            <div className={'container'}>
                <TodoContainer />
            </div>
        </Provider>
    )
};

export default App;

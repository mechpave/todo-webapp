<?php

namespace App;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    /** @var string */
    private $email;

    /** @var string */
    private $password;

    /**
     * @param string $email
     * @param string $password
     */
    public function __construct(string $email, ?string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername(): string
    {
        return $this->email;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }


    /**
     * {@inheritdoc}
     */
    public function eraseCredentials(): void
    {
    }

    /**
     * {@inheritoc}
     */
    public function getSalt(): ?string
    {
        return null;
    }
}

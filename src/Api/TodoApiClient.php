<?php

namespace App\Api;

use GuzzleHttp\Client;

class TodoApiClient
{
    /** @var Client */
    private $httpClient;

    /**
     * @param Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function sendRequest(string $method, string $path, Credentials $credentials, array $parameters = [])
    {
        $response = $this->httpClient->request(
            $method,
            'api/' . $path,
            [
                'auth' => [
                    $credentials->getEmail(),
                    $credentials->getPassword(),
                ],
                'json' => $parameters,
            ]
        );

        $result = json_decode($response->getBody()->getContents(), 'true');

        return $result;
    }

    public function areCredentialsValid(Credentials $credentials): bool
    {
        try {
            $response = $this->httpClient->get(
                'api/todo-items',
                [
                    'auth' => [$credentials->getEmail(), $credentials->getPassword()],
                ]
            );
        } catch (\Throwable $e) {
            return false;
        }

        return $response->getStatusCode() === 200;
    }
}

<?php

namespace App\Controller;

use App\Api\Credentials;
use App\Api\TodoApiClient;
use App\User;
use GuzzleHttp\Exception\RequestException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    /** @var TodoApiClient */
    private $apiClient;

    /**
     * @param TodoApiClient $apiClient
     */
    public function __construct(TodoApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function index()
    {
        return $this->render('Default/index.html.twig', []);
    }

    /**
     * Redirects requests from React app to java backend.
     *
     * @param Request $request
     * @param string  $route
     * @return JsonResponse
     */
    public function apiRedirect(Request $request, string $route)
    {
        /** @var User $user */
        $user = $this->getUser();

        try {
            $result = $this->apiClient->sendRequest(
                $request->getMethod(),
                $route,
                new Credentials($user->getUsername(), $user->getPassword()),
                json_decode($request->getContent(), true) ?? []
            );
        } catch (RequestException $e) {
            return new JsonResponse(
                json_decode($e->getResponse()->getBody()->getContents(), true),
                $e->getResponse()->getStatusCode()
            );
        } catch (\Throwable $e) {
            return new JsonResponse([], 500);
        }

        return new JsonResponse($result);
    }
}
